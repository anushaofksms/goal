import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-goal-list',
  templateUrl: './goal-list.component.html',
  styleUrls: ['./goal-list.component.css']
})
export class GoalListComponent implements OnInit {
  
  @Input() goalDetails;
  @Output() clickData = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
    // console.log('goal-list',this.goalYearWiseData)
  }
  
  keyResultKey(result)
  {
    this.clickData.emit(result)
    //  console.log()
  }
}
