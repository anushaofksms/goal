import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-my-goal-plan',
  templateUrl: './my-goal-plan.component.html',
  styleUrls: ['./my-goal-plan.component.css']
})
export class MyGoalPlanComponent implements OnInit {
  @Input() goalData;
  @Output() Yearlydata = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  filterDataByDate(value)
  {
     this.Yearlydata.emit(value);
  }

}
