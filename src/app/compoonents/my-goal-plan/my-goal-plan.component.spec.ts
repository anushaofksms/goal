import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyGoalPlanComponent } from './my-goal-plan.component';

describe('MyGoalPlanComponent', () => {
  let component: MyGoalPlanComponent;
  let fixture: ComponentFixture<MyGoalPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyGoalPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyGoalPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
