import { Component, OnInit } from '@angular/core';
import { filter } from 'minimatch';
declare var require:any;

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.css']
})
export class GoalsComponent implements OnInit {
  
  goalData:any;
  goalYearWiseData:any;
  showKeyResult:any;
  constructor() { }

  ngOnInit() {
     this.goalData = require('./goal-data.json');
     this.goalYearWiseData = this.goalData[0];
  }

  getYearlyData(event)
  {
    // for(var i = 0; i<this.goalData.length;i++)
    // {
    //   if(this.goalData[i].year == event)
    //   {
    //       this.goalYearWiseData = this.goalData[i];
    //   }
    // }
    const yearData = this.goalData.filter(item=>
      item.year == event
     )
     this.goalYearWiseData = yearData[0];
    //  console.log(this.goalYearWiseData.goal_details[0].goal_key_result)
  }

  clickArrow(event)
  {
     event.isCollapse = !event.isCollapse;
  }
}
