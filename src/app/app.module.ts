import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { GoalsComponent } from './containers/goals/goals.component';
import { MyGoalPlanComponent } from './compoonents/my-goal-plan/my-goal-plan.component';
import { CheckInComponent } from './compoonents/check-in/check-in.component';
import { GoalViewComponent } from './compoonents/goal-view/goal-view.component';
import { TotalGoalsComponent } from './compoonents/total-goals/total-goals.component';
import { GoalListComponent } from './compoonents/goal-list/goal-list.component';
import { KeyResultComponent } from './compoonents/key-result/key-result.component';

const appRoutes: Routes = [
  { path: '',   redirectTo: '/goal', pathMatch: 'full' },
  { path: 'goal', component: GoalsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    GoalsComponent,
    MyGoalPlanComponent,
    CheckInComponent,
    GoalViewComponent,
    TotalGoalsComponent,
    GoalListComponent,
    KeyResultComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
